// Asynchronous from nodejs.org
const crypto = require('crypto').randomBytes(256).toString('hex');

// Expordin kogu objekti
module.exports = {
    // uri: 'mongodb://localhost:27017/mean-angular-2', // seda on vaja kohaliku serveri jaoks
    uri: 'mongodb://martin:kirjakastid@ds233895.mlab.com:33895/angular-2-app', // Production Heroku jaoks
    secret: crypto,
    // db: 'mean-angular-2' // kohaliku serveri jaoks
    db: 'angular-2-app' // Production Database mlabis
}