import { RouterModule, Routes } from '@angular/router'; // selle leidsin angulari kodukalt https://angular.io/guide/router
import {NgModule} from '@angular/core';
import {HomeComponent} from './components/home/home.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BlogComponent } from './components/blog/blog.component';
import { SinglePostComponent } from './components/blog/single-post/single-post.component';
import { EditBlogComponent } from './components/blog/edit-blog/edit-blog.component';
import { DeleteComponent } from './components/blog/delete/delete.component';

import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/notAuth.guard';


const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  //  canActivate: [AuthGuard] // kui kasutaja on sisse loginud, saab sellele ligi
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [NotAuthGuard] // kui kasutaja on sisse loginud, siis sellele ligi ei pääse
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NotAuthGuard] // kui kasutaja on sisse loginud, siis sellele ligi ei pääse
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard] // kui kasutaja on sisse loginud, saab sellele ligi
  },
  {
    path: 'blog',
    component: BlogComponent
    // canActivate: [AuthGuard] // kui kasutaja on sisse loginud, saab sellele ligi
  },
  {
    path: 'single-post/:id',
    component: SinglePostComponent
  },
  {
    path: 'edit-blog/:id',
    component: EditBlogComponent,
    canActivate: [AuthGuard] // kui kasutaja on sisse loginud, saab sellele ligi
  },
  {
    path: 'delete/:id',
    component: DeleteComponent,
    canActivate: [AuthGuard] // kui kasutaja on sisse loginud, saab sellele ligi
  },
  {path: '*', component: HomeComponent}
];

// alljärgneva copy-pastesin app.module failist
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes)],
  providers: [],
  bootstrap: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
