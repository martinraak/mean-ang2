// selle põhja kopeerisin auth.guard.ts failist

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class NotAuthGuard implements CanActivate {
    constructor(
        private authService: AuthService,
        private router: Router
      ) { }

      // kui kasutaja on sisselogitud, saab kindlatele routidele ligi, kui mitte, suunab logini lehele
  canActivate() {
if (this.authService.loggedIn()) {
    this.router.navigate(['/']);
    return false;
} else {
return true;
}
}
}
