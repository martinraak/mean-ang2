// selle sain siit - https://angular.io/guide/router

import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    redirectUrl; // see saab olema see URL, kuhu kasutaja pärast redirectitakse (see, millele ta ligi tahtis pääseda enna sisselogimist)

    constructor(
        private authService: AuthService,
        private router: Router
      ) { }

      // kui kasutaja on sisselogitud, saab kindlatele routidele ligi, kui mitte, suunab logini lehele

      canActivate(
      router: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ) {
if (this.authService.loggedIn()) {
    return true;
} else {
    this.redirectUrl = state.url; // siit võtame selle, kuhu kasutaja algselt tahtis minna
this.router.navigate(['/login']);
return false;
}
}
}
