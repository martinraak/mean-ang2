import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

// kasutan seda selleks, et kui kasutaja on sisselogitud, siis ei näitaks navbaris login nuppu
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

    domain = ''; // Heroku jaoks
  // domain = 'http://localhost:8080/'; // development domain. seda pole vaja Productioni jaoks
  authToken;
  user;
  options;

  constructor(
    private http: Http

  ) { }

  createAuthenticationHeaders() {
    this.loadToken();
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'authorization': this.authToken
      })
    });
  }

  loadToken() {
    const token = localStorage.getItem('token');
    this.authToken = token;
  }

  registerUser(user) {
return this.http.post(this.domain + 'authentication/register', user).map(res => res.json());

  }

  // kontrollin, kas username on juba olemas kasutades funktsiooni authentication.js-is
  checkUsername(username) {
    return this.http.get(this.domain + 'authentication/checkUsername/' + username).map(res => res.json());

  }

// kontrollin, kas e-mail on juba olemas kasutades funktsiooni authentication.js-is
  checkEmail(email) {
    return this.http.get(this.domain + 'authentication/checkEmail/' + email).map(res => res.json());

  }

  login(user) {
    return this.http.post(this.domain + 'authentication/login', user).map(res => res.json());
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  // jätan meelde, kes sisse on loginud
  storeUserData(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  // vaatan, kes on sisselogitud, et saaksin tema andmeid nt. profiili all kuvada
  getProfile() {
this.createAuthenticationHeaders();
return this.http.get(this.domain + 'authentication/profile', this.options).map(res => res.json());
  }

// see samuti siis selleks, et loginit kaotada navbarist, kui user on sisselogitud
loggedIn() {
  return tokenNotExpired();
}

}
