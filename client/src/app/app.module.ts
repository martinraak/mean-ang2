import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RegisterComponent } from './components/register/register.component';
import { HttpModule } from '@angular/http';

import {AuthService} from './services/auth.service';
import { BlogService } from './services/blog.service';

import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/notAuth.guard';
import { BlogComponent } from './components/blog/blog.component';
import { SinglePostComponent } from './components/blog/single-post/single-post.component';
import { EditBlogComponent } from './components/blog/edit-blog/edit-blog.component';
import { DeleteComponent } from './components/blog/delete/delete.component';
import { TextEditorComponent } from './components/helpers/text-editor/text-editor.component';
import { RightColumnComponent } from './components/right-column/right-column.component';

import { QuillModule } from 'ngx-quill';
import { EscapeHtmlPipe } from './components/helpers/keep-html.pipe';
import { ShareModule } from 'ng2share/share.module';
import { ExcerptFilter } from './components/blog/excerpt.filter';
import {TimeAgoPipe} from 'time-ago-pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    DashboardComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    BlogComponent,
    SinglePostComponent,
    EditBlogComponent,
    DeleteComponent,
    RightColumnComponent,
    TextEditorComponent,
    EscapeHtmlPipe,
    ExcerptFilter,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FlashMessagesModule,
    QuillModule,
    ShareModule
    ],
  providers: [AuthService, AuthGuard, NotAuthGuard, BlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
