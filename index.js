const express = require('express');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const config = require('./config/database'); // impordin Mongoose configi mooduli
const path = require('path');
const authentication = require('./routes/authentication')(router);
const blogs = require('./routes/blogs')(router);
const bodyParser = require('body-parser'); // selle installisin siit https://github.com/expressjs/body-parser et fronti backi tõlgendada
const cors = require('cors'); // corsi installisin käsuga 'npm install cors --save' selleks, et saaksin serverit ruutida
const port = process.env.PORT || 8080;

// loen selle uri siis database.js failist
mongoose.Promise = global.Promise;
mongoose.connect(config.uri);

// Middleware

app.use(cors({
    origin: 'http://localhost:4200', // ütlen talle, kus asub development server. seda vaja ainult siis, kui kohaliku andmebaasi otsas olen
}));

// body-parseri githubist: parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));
app.use('/authentication', authentication);
app.use('/blogs', blogs);

// Connect server to Angular 2 Index.html
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/index.html')); // seda on vaja Herokuapi jaoks
    // res.sendFile(path.join(__dirname + '/client/src/index.html')); // seda on vaja kohaliku serveri jaoks
});

// Start server listen on port 8080
app.listen(port, () => {
    console.log('Listening on port ' + port);
});