// selle copysin siit: http://mongoosejs.com/docs/guide.html
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

// parooli krüpteerimiseks kasutan https://www.npmjs.com/package/bcrypt-nodejs
const bcrypt = require('bcrypt-nodejs');

// kui kasutaja sisestab emaili tühikud, siis ei luba registreerida
let emailLengthChecker = (email) => {
    if (!email) { // kui e-maili ei ole
        return false;
    } else {
        if (email.length < 5 || email.length > 40) {
            return false;
        } else {
            return true;
        }
    }
}

// kui kasutaja ei sisesta e-mail korrektselt, siis ei luba registreerida. RegExp sisu sain õpetusest, kes omakorda oli selle kusagilt netist tõmmanud
let validEmailChecker = (email) => {
    if (!email) { // kui e-maili ei ole
        return false;
    } else {
        const regExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return regExp.test(email); // annab vastuseks true või false, sõltuvalt, kas täidab ära ülevalasuva criteria
    }
};

// teen emaili validaatorite massiivi
const emailValidators = [{
        validator: emailLengthChecker,
        message: 'E-mail must be at least 5 characters but no more than 40.'
    },
    {
        validator: validEmailChecker,
        message: 'Hey, your e-mail must be a valid one. Try again.'
    }
]

// valideerin ka kasutajanime
let usernameLengthChecker = (username) => {
    if (!username) {
        return false;
    } else {
        if (username.length < 3 || username.length > 15) {
            return false;
        } else {
            return true;
        }
    }
};

// valideerin, et kasutataks õigeid tähemärke
let validUsername = (username) => {
    if (!username) {
        return false;
    } else {
        const regExp = new RegExp(/^[a-zA-Z0-9]+$/);
        return regExp.test(username);
    }
};

// teen kasutajnimede validaatori massiivi
const usernameValidators = [
    // esimene Username validator
    {
        validator: usernameLengthChecker,
        message: 'Username must be at least 3 characters but no more than 15'
    },
    // teine username validator
    {
        validator: validUsername,
        message: 'Username must not have any special characters'
    }
];

// Validate funktsioon, kus kontrollin parooli pikkust
let passwordLengthChecker = (password) => {
    // Check if password exists
    if (!password) {
        return false; // Return error
    } else {
        // Check password length
        if (password.length < 8 || password.length > 35) {
            return false; // Return error if password length requirement is not met
        } else {
            return true; // Return password as valid
        }
    }
};

// passwordi formaadi validaatori funktsioon
let validPassword = (password) => {
    // Check if password exists
    if (!password) {
        return false; // Return error
    } else {
        // Regular Expression to test if password is valid format
        const regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
        return regExp.test(password); // Return regular expression test result (true or false)
    }
};

// passwordi validaatorite massiiv
const passwordValidators = [
    // First password validator
    {
        validator: passwordLengthChecker,
        message: 'Password must be at least 8 characters but no more than 35'
    },
    // Second password validator
    {
        validator: validPassword,
        message: 'Must have at least one uppercase, lowercase, special character, and number'
    }
];


const userSchema = new Schema({
    email: { type: String, required: true, unique: true, lowercase: true, validate: emailValidators },
    username: { type: String, required: true, unique: true, lowercase: true, validate: usernameValidators },
    password: { type: String, required: true, unique: true, validate: passwordValidators }
});

// nüüd enne kui ükski ülemine schema saab salvestatud, encryptime parooli. koodi saab bcrypti kodukalt

userSchema.pre('save', function(next) {
    if (!this.isModified('password')) // kui parooli väli pole muudetud, siis ei pea seda middleware siin jooksutama
        return next();

    bcrypt.hash(this.password, null, null, (err, hash) => {
        if (err) return next(err);
        this.password = hash; // kui kõik korras, assigni hash
        next(); // exit the middleware
    })

});

// võrdlen paroole
userSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

// selle sain samuti mongoose'i guide'ist
module.exports = mongoose.model('User', userSchema);