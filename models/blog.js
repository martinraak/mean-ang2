// copy-pastesin selle faili põhja user.js-ist
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

// kui kasutaja sisestab emaili tühikud, siis ei luba registreerida
let titlelLengthChecker = (title) => {
    if (!title) {
        return false;
    } else {
        if (title.length < 3 || title.length > 500) {
            return false;
        } else {
            return true;
        }
    }
}

// teen emaili validaatorite massiivi
const titleValidators = [{
    validator: titlelLengthChecker,
    message: 'Title must be at least 3 characters but no more than 50.'
}]

// valideerin ka kasutajanime
let bodyLengthChecker = (body) => {
    if (!body) {
        return false;
    } else {
        if (body.length < 5 || body.length > 5000) {
            return false;
        } else {
            return true;
        }
    }
};

// teen kasutajnimede validaatori massiivi
const bodyValidators = [
    // esimene Username validator
    {
        validator: bodyLengthChecker,
        message: 'Body must be at least 5 characters but no more than 5000'
    }
];

// Validate funktsioon, kus kontrollin parooli pikkust
let commentLengthChecker = (comment) => {
    // Check if password exists
    if (!comment[0]) {
        return false; // Return error
    } else {
        // Check password length
        if (comment[0].length < 1 || comment[0].length > 200) {
            return false; // Return error if password length requirement is not met
        } else {
            return true; // Return password as valid
        }
    }
};

// passwordi validaatorite massiiv
const commentValidators = [
    // First password validator
    {
        validator: commentLengthChecker,
        message: 'Comments may not exceed 200 characters'
    }
];

const blogSchema = new Schema({
    title: {
        type: String,
        required: true,
        validate: titleValidators
    },
    body: {
        type: String,
        required: true,
        validate: bodyValidators
    },
    createdBy: { type: String },
    createdAt: { type: Date, default: Date.now() },
    comments: [{
        comment: {
            type: String,
            validate: commentValidators,
        },
        commentator: {
            type: String
        },
        commentedAt: {
            type: Date,
            default: Date.now()
        }
    }],
    // lisasin kategooria juurde
    category: { type: String }
});


// selle sain samuti mongoose'i guide'ist
module.exports = mongoose.model('Blog', blogSchema);