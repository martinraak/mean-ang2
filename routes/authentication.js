const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

module.exports = (router) => {

    router.post('/register', (req, res) => {
        // req.body.email;
        // req.body.username;
        // req.body.password;
        if (!req.body.email) { // kui e-maili pole
            res.json({ success: false, message: 'You have to provide an e-mail, man!' });
        } else {
            if (!req.body.username) { // kui kasutajanime pole
                res.json({ success: false, message: 'You have to provide a username, man!' });
            } else {
                if (!req.body.password) { // kui parooli pole
                    res.json({ success: false, message: 'You have to provide a password, man!' });
                } else {
                    let user = new User({
                        email: req.body.email.toLowerCase(),
                        username: req.body.username.toLowerCase(),
                        password: req.body.password
                    });
                    user.save((err) => {
                        if (err) {
                            if (err.code === 11000) { // kui saan errori 11000, mis tähendab, et username või email on juba olemas
                                res.json({ success: false, message: 'Could not save user, she already exists. Try again.' });
                            } else {

                                if (err.errors) {
                                    if (err.errors.email) {
                                        res.json({ success: false, message: err.errors.email.message });
                                    } else {
                                        if (err.errors.username) {
                                            res.json({ success: false, message: err.errors.username.message });
                                        } else {
                                            if (err.errors.password) {
                                                res.json({ success: false, message: err.errors.password.message });
                                            } else {
                                                res.json({ sucess: false, message: err });
                                            }
                                        }
                                    }
                                } else {
                                    res.json({
                                        success: false,
                                        message: 'Could not save user. Error',
                                        err
                                    });
                                }
                            }
                        } else {
                            res.json({ success: true, message: 'User saved successfully' });
                        }
                    });
                }
            }
        }
    });

    // teen routeri, et kasutaja saaks juba enne submittimist teada, et see e-mail on juba kasutuses, lisan pärast auth.service'isse
    router.get('/checkEmail/:email', (req, res) => {
        if (!req.params.email) {
            res.json({ sucess: false, message: 'E-mail was not provided' });
        } else {
            User.findOne({ email: req.params.email }, (err, user) => {
                if (err) {
                    res.json({ success: false, message: err });
                } else {
                    if (user) {
                        res.json({ success: false, message: 'E-mail is already taken' });
                    } else {
                        res.json({ success: false, message: 'E-mail is available' });
                    }
                }
            });
        }
    });

    // teen samasuguse routeri, et kasutaja saaks juba enne submittimist teada, et see kasutajanimi on juba kasutuses, lisan pärast auth.service'isse
    router.get('/checkUsername/:username', (req, res) => {
        if (!req.params.username) {
            res.json({ sucess: false, message: 'Username was not provided' });
        } else {
            User.findOne({ username: req.params.username }, (err, user) => {
                if (err) {
                    res.json({ success: false, message: err });
                } else {
                    if (user) {
                        res.json({ success: false, message: 'Username is already taken' });
                    } else {
                        res.json({ success: false, message: 'Username is available' });
                    }
                }
            });
        }
    });

    router.post('/login', (req, res) => {
        if (!req.body.username) {
            res.json({ success: false, message: 'No username was provided' });
        } else {
            if (!req.body.password) {
                res.json({ success: false, message: 'No password was provided' });
            } else {
                User.findOne({ username: req.body.username.toLowerCase() }, (err, user) => {
                    if (err) {
                        res.json({ success: false, message: err });
                    } else {
                        if (!user) {
                            res.json({ success: false, message: 'Username was not found' });
                        } else {
                            const validPassword = user.comparePassword(req.body.password);
                            if (!validPassword) {
                                res.json({ success: false, message: 'Password does not match' });
                            } else {
                                const token = jwt.sign({ // nüüd teen nii, et kui oled sisse loginud, siis jääd ka sisse
                                    userId: user._id
                                }, config.secret, { expiresIn: '24h' }); // 24h pärast logitakse automaatselt välja
                                res.json({ success: true, message: 'Success!', token: token, user: { username: user.username } });
                            }
                        }
                    }
                });
            }
        }
    });

    // võtan välja authentication data, et saaksin sisselogitud useri kasutajanime näidata profiili all näiteks

    // see on Middleware
    router.use((req, res, next) => {
        const token = req.headers['authorization'];
        //   if (!token) {
        //       res.json({ success: false, message: 'No token provided' });
        //   } else {
        jwt.verify(token, config.secret, (err, decoded) => {
            //    if (err) {
            //        res.json({ success: false, message: 'Token invalid: ' + err });
            //    } else {
            req.decoded = decoded;
            next();
            //    }
        });
        //  }
    });

    router.get('/profile', (req, res) => {
        User.findOne({ _id: req.decoded.userId }).select('username email').exec((err, user) => {
            if (err) {
                res.json({ success: false, message: err });
            } else {
                if (!user) {
                    res.json({ success: false, message: 'User not found' });
                } else {
                    res.json({ success: true, user });
                }
            }
        });
    });

    return router;
}